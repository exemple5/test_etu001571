/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Annotation.MethodUrl;
import Model.Personne;
import java.util.ArrayList;
import java.util.HashMap;
import util.ModelView;

/**
 *
 * @author Mahatoky Ny Avo
 */
public class PersonneController {
    
    @MethodUrl(url = "liste")
    public ModelView liste(){
        ArrayList<Personne> list = new ArrayList();
        list.add(new Personne("RANDRIANATOANDRO","Myranto",19));
        list.add(new Personne("RAFALIMANANA","Mahatoky",19));
        list.add(new Personne("RAMANAMPAMONJY","Lahatra",19));
        list.add(new Personne("RATOVOJANAHARY","Larousso",21));
        list.add(new Personne("RAKOTOMANGA","Ivan",22));
        HashMap hm = new HashMap();
        hm.put("ls", list);
        ModelView res = new ModelView("liste.jsp", hm);
        return res;
    }
    
    @MethodUrl(url = "ajoutPersonne")
    public ModelView loadForm(){
        ModelView res = new ModelView("form.jsp", null);
        return res;
    }
    
    @MethodUrl(url = "affiche")
    public ModelView affiche(){
        ModelView res = new ModelView("affichePersonne.jsp", null);
        return res;
    }
}
