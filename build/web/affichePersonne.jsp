<%-- 
    Document   : affichePersonne
    Created on : Nov 18, 2022, 2:35:19 PM
    Author     : Mahatoky Ny Avo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
            out.println("<p>Nom: "+request.getAttribute("nom")+"</p>");
            out.println("<p>Prenom: "+request.getAttribute("prenom")+"</p>");
            out.println("<p>Age "+request.getAttribute("age")+"</p>");
        %>
    </body>
</html>
