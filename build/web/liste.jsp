<%-- 
    Document   : liste
    Created on : Nov 17, 2022, 8:19:40 PM
    Author     : Mahatoky Ny Avo
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="Model.Personne"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    ArrayList<Personne> list = new ArrayList();
    list = (ArrayList)request.getAttribute("ls");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Page</title>
    </head>
    <body>
        <h1>La liste des personnes</h1>
        <%
            for(int i=0;i<list.size();i++){
                 out.println("<p>Nom: "+list.get(i).getNom()+"<br>");
                 out.println("Prenom: "+list.get(i).getPrenom()+"<br>");
                 out.println("Age: "+list.get(i).getAge()+"</p>");
             }
        %>
    </body>
</html>
